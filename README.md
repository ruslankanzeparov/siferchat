"End-to-end encrypted group chat."

Group chat for messaging with end-to-end encryption in which several participants need access to read and write to the channel. Participants can be easily added or removed to the chat by issuing or revoking the NuCypher re-encryption token.

The functionality should allow you to create group and personal chats. In this case, permissions to read and write to the chat are issued using re-encryption tokens. The chat administrator implements and revokes these tokens.

Technical limitations:
 Implemented a prototype chat, as a mobile application for android. Implemented backend using NuCypher platform encryption. Connection of parts as possible.
 
 use android studio and run project